#include <iostream>
#include <string>
#include <algorithm>

using namespace std;


int main() {
	string a;
	string b;
	cin >> a;
	cin >> b;
	transform(a.begin(), a.end(), a.begin(), ::tolower);
	transform(b.begin(), b.end(), b.begin(), ::tolower);
	if (lexicographical_compare(a.begin(), a.end(), b.begin(), b.end())) {
		cout << "-1\n";
	}
	else if (lexicographical_compare(b.begin(), b.end(), a.begin(), a.end())) {
		cout << "1\n";
	}
	else {
		cout << "0\n";
	}
}
