#include <iostream>

using namespace std;

int main() {
	int w;
	cin >> w;
	bool can = false;

	for (int i=0;i<w;i+=2) {
		for (int j=0;j<w;j+=2) {
			if (i+j == w) {
				can = true;
				break;
			}
		}
	}
	if (can) {cout << "YES\n";}
	else {cout << "NO\n";}
}
