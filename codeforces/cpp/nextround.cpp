#include <iostream>

using namespace std;

int main() {
	int n,k,c = 0;
	cin >> n >> k;
	int seq[n];
	for (int x=0;x<n;x++) {
		int e;
		cin >> e;
		seq[x] = e;
	}
	for (int x=0;x<n;x++) {
		if (seq[x] > 0) {
			if (seq[x] >= seq[k-1]) {
				c++;
			}
		}
	}
	cout << c << "\n";
}
