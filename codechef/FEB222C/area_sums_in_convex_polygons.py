def parea(m:list):
    s = len(m)
    sm = 0
    for i in range(1,s):
        sm += ( pow(-1, i+1) * (m[i][0] * m[i+1][1] - m[i+1][0] * m[i][1]) )
    return sm
t = int(input())
for z in range(t):
    n = int(input())
    points = []
    for _ in range(n):
        t = [int(x) for x in input().split()]
        points.append(t)
    sm = 0
    for i in range(n):
        for j in range(n):
            if i<j:
                if not (i-1==j or j-1==i):
                    sm += min(parea(points[i:j]), parea(points[j:i]))
