#include <iostream>
#include <string>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int x;
		cin >> x;
		string s;
		cin >> s;
		int carlsen=0;
		int chef=0;
		for (int i=0;i<14;i++) {
			if (s[i] == 'C') {
				carlsen+=2;
			}
			else if (s[i] == 'N') {
				chef+=2;
			}
			else if (s[i] == 'D') {
				carlsen++;
				chef++;
			}
		}
		if (carlsen>chef) {
			cout << 60*x << "\n";
		}
		else if (chef>carlsen) {
			cout << 40*x << "\n";
		}
		else if (chef == carlsen) {
			cout << 55*x << "\n";
		}
	}
}
