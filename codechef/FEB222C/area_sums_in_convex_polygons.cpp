#include <iostream>
#include <vector>
#include <math.h>
using namespace std;


int parea(vector<vector<int>> &m) {
	int s = m.size();
	int sum = 0;
	for (int i=1;i<s;i++) {	
		sum += ( pow(-1, i+1) * (m[i][0] * m[i+1][1] - m[i+1][0] * m[i][1])  );
	}
	return sum;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		vector<vector<int>> points( n , vector<int> (2, 0));
		for (int i=0;i<n;i++) {
			int x,y;
			cin >> x >> y;
			points[i][0] = x;
			points[i][1] = y;	
		}
		int sum = 0;
		for (int i=0;i<n;i++) {
			for (int j=0;j<n;j++) {
				if (i<j) {
					if (!(i-1==j || j-1==i)) {
						vector<vector<int>> one(points.begin()+i, points.begin()+j);
						vector<vector<int>> two(points.begin()+j, points.begin()+i);
						sum += min(parea(one), parea(two));
					}
				}
			}
		}
		cout << sum << "\n";
	}
}
