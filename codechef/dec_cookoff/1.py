t = int(input())
tests = []
for _ in range(t):
    tests.append([int(c) for c in input().split()])

ans = []

def solve(x,y,z):
    if (x+y)<z:
        return ("PLANEBUS")
    elif (x+y)>z:
        return ("TRAIN")
    else:
        return ("EQUAL")

for test in tests:
    ans.append(solve(test[0], test[1], test[2]))

for a in ans:
    print(a)