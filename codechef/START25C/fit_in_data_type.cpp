#include <iostream>
using namespace std;
int main() {
	int t;
	cin >> t;
	for (int e=0;e<t;e++) {
		int n,x;
		cin >> n >> x;
		if (x <= n) {
			cout << x << "\n";
		}
		else {
			cout << x-n-1 << "\n";
		}
	}
}
