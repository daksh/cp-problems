#include <cstddef>
#include <iostream>
#include <unordered_set>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--){
		int n;
		cin >> n;
		int xs[n];
		int ys[n];
		for (int i=0;i<n;i++) {
			int x,y;
			cin >> x >> y;
			xs[i]=x;
			ys[i]=y;	
		}
		const size_t lenx = sizeof(xs)/sizeof(xs[0]);
		const size_t leny = sizeof(ys)/sizeof(ys[0]);
		unordered_set<int> xs_set(xs, xs+lenx);
		unordered_set<int> ys_set(ys, ys+leny);

				
	}
}
