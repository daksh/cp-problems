for _ in range(int(input())):
    n = int(input())
    seq = set()
    xs = set()
    ys = set()
    for z in range(n):
        s = input()
        seq.add(s)
        s = [int(x) for x in s.split()]
        xs.add(s[0])
        ys.add(s[1])
    print(len(xs) + len(ys) - len(seq))
    print("-"*10)
    print(xs, ys, seq)
    print("-"*10)
