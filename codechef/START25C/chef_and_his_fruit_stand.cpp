#include <iostream>
#include <math.h>
using namespace std;

int main() {
	int t;
	cin >> t;
	for (int i=0;i<t;i++) {
		int x,y;
		cin >> x >> y;
		if (x>y && x/2>y) {
			cout << y << "\n";
		}
		else {
			cout << floor(x/2)  << "\n";
		}
	}
}
