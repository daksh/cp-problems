#include <iostream>
#include <math.h>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--)	{
		int n;
		cin >> n;
		cout << ceil(n/10) << "\n";
	}
}
