#include <iostream>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int a,b;
		cin >> a >> b;
		if (a+b > 20) {
			cout << "-1\n";
		}
		else if (21-(a+b) > 10) {
			cout << "-1\n";
		}
		else {
			cout << 21-(a+b) << "\n";
		}
	}	
}
