t=int(input())
for _ in range(t):
    i=[int(x) for x in input().split()]
    n=i[0]
    w=i[1]
    a=[int(x) for x in input().split()]
    a.sort()
    h=0
    while (sum(a[1::]) > w):
        del a[0]
        h+=1
    print(h)
