n = int(input())
s = input().split()
p = input().split()


s = [x if x!="?" else alpha for x in s]
p = [x if x!="?" else alpha for x in p]

def is_vowel(char):
    return char in ["a", "e", "i", "o", "u"]

cmd = 0

for x in range(len(p)):
    if not p[x] == s[x]:
        if p[x] in vowels:
            if not s[x] in vowels:
                p[x] = s[x]
                cmd += 1
        else:
            if [x] in vowels:
                p[x] = s[x]
                cmd += 1

print(cmd)