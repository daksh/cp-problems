t = int(input())
tests = []
ans = []

def max_cash(a):
    #n = int(input())
    #a = [int(x) for x in input().split()]
    a.sort()
    max_cash = sum(a) - a[0]
    return max_cash

for x in range(t):
    tests.append({"n":int(input()), "a":[int(x) for x in input().split()]})

for case in tests:
    ans.append(max_cash(case["a"]))

for an in ans: print(an)