t = int(input())
for _ in range(t):
    i = [int(x) for x in input().split()]
    n = i[0]
    x = i[1]
    y = i[2]
    m = 1000006
    for i in range(n+1):
        for j in range(n+1):
            k = 100*j+4*i
            if k>=n:
                m=min(m,i*y+j*x)
            else:
                m = 0
    print(m)
