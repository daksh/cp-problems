t = int(input())
for _ in range(t):
    inp = [int(x) for x in input().split()]
    n = inp[0]
    x = inp[1]
    s = [int(x) for x in input().split()] # len: n

    m = x
    buf = x
    for i in s:
        buf += i
        if buf>m:
            m = buf
    print(m)
