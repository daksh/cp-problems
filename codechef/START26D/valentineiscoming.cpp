#include <iostream>
#include <cmath>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int x,y;
		cin >> x >> y;
		cout << round(x/y) << "\n";
	}
}
