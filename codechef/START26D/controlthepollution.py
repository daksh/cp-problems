from math import ceil
t = int(input())
for _ in range(t):
    i = [int(x) for x in input().split()]
    n = i[0]
    x = i[1] # bus
    y = i[2] # car
    
    m = ceil(0/100)*x+ceil((n-0)/100)*y
    for k in range(1, n+1):
        g = ceil(k/100)*x+ceil((n-k)/100)*y
        if g<m:
            m = g
    print(m)
